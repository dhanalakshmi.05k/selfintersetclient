import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ApiConfigService} from "../configservice/api-config.service";

@Injectable({
  providedIn: 'root'
})
export class UserManagementService {

  constructor(private http: HttpClient, public configService: ApiConfigService) {
  }

  saveUserDetails(userDetails) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'addUserDetails', userDetails);
  }

  getAllUser(start,end) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allUsers/'+ start+'/'+end);
  }

  getUserByMongodbId(userId) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'userById/' + userId);
  }
  getUserByEmailId(emailId) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'userByEmailId/' + emailId);
  }
  updateUserById(userId, userDetails) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'editUserDetails/' + userId, userDetails);
  }
  updateUserByEmailID(emailId, userDetails) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'editUserDetailsByEmailId/' + emailId, userDetails);
  }

  deleteUserByMongodbId(userId) {
    return this.http.delete(this.configService.appConfig.appBaseUrl + 'userByMongoId/' + userId);
  }

  getUserCount() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allUsers/count');
  }



  sendForgotPasswordReuest(userDetails) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'forgotPassword', userDetails);
  }

}
