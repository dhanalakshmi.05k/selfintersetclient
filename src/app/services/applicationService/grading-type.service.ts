import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ApiConfigService} from "../configservice/api-config.service";

@Injectable({
  providedIn: 'root'
})
export class GradingTypeService {

  constructor(private http: HttpClient, public configService: ApiConfigService) {
    }
  
    saveGradingTypeDetails(gradingTypeDetails) {
      return this.http.post(this.configService.appConfig.appBaseUrl + 'addGradingType', gradingTypeDetails);
    }
  
    getAllGradingTypes(start,end) {
      return this.http.get(this.configService.appConfig.appBaseUrl + 'allGradingType/'+ start+'/'+end);
    }
  
    getGradingTypeByMongodbId(gradingTypeId) {
      return this.http.get(this.configService.appConfig.appBaseUrl + 'gradingTypeById/' + gradingTypeId);
    }
  
    updateGradingTypeById(gradingTypeId, gradingTypeData) {
      return this.http.post(this.configService.appConfig.appBaseUrl + 'editGradingTypesById/' + gradingTypeId, gradingTypeData);
    }

    deleteGradingTypeByMongodbId(gradingTypeId) {
      return this.http.delete(this.configService.appConfig.appBaseUrl + 'deleteGradingType/' + gradingTypeId);
    }
  
    getGradingTypeCount() {
      return this.http.get(this.configService.appConfig.appBaseUrl + 'allGradingType/count');
    }
  
  }
