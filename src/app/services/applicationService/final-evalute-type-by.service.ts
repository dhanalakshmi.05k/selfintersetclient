import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ApiConfigService} from "../configservice/api-config.service";

@Injectable({
  providedIn: 'root'
})
export class FinalEvaluteTypeByService {

  constructor(private http: HttpClient, public configService: ApiConfigService) {
  }

  saveEvalutionFinalMarkByDetails(markByObj) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'addAssignmentEvaluationMarkBy',markByObj);
  }

  getAllAssignmentEvalutionFinalMarkBy(start,end) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentEvaluationMarkBy/'+start
      +'/'+end );
  }

  getAssignmentEvalutionFinalMarkByMongodbId(assignmentTypeId) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'assignmentEvaluationMarkById/'
      + assignmentTypeId);
  }

  updateAssignmentEvalutionFinalMarkById(assignmentTypeId, assignmentTypeData) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'editAssignmentEvaluationByIdMarkBy/'
      + assignmentTypeId, assignmentTypeData);
  }


  deleteAssignmentEvalutionFinalMarkByMongodbId(assignmentTypeId) {
    return this.http.delete(this.configService.appConfig.appBaseUrl + 'deleteAssignmentEvaluationMarkBy/'
      + assignmentTypeId);
  }

  getEvalutionFinalMarkByCount() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentEvaluationMarkBy/count');
  }
}
