import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ApiConfigService} from "../configservice/api-config.service";

@Injectable({
  providedIn: 'root'
})
export class SemisterDetailsService {

  constructor(private http: HttpClient, public configService: ApiConfigService) {
  }

  saveSemisterDetailsDetails(semisterDetailsDetails) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'addsemisterDetails', semisterDetailsDetails);
  }

  getAllSemister(skip,limit) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allSemister'+'/'+skip+'/'+limit );
  }

  getSemisterByMongodbId(semisterId) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'semisterById/' + semisterId);
  }

  updateSemisterById(semisterId, semisterData) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'editSemisterById/' + semisterId, semisterData);
  }


  deleteSemisterByMongodbId(semisterId) {
    return this.http.delete(this.configService.appConfig.appBaseUrl + 'deleteSemister/' + semisterId);
  }

  getSemisterCount() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allSemister/count');
  }

}
