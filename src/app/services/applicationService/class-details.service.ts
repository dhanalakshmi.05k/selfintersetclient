import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ApiConfigService} from "../configservice/api-config.service";

@Injectable({
  providedIn: 'root'
})
export class ClassDetailsService {

  constructor(private http: HttpClient, public configService: ApiConfigService) {
  }

  saveClassDetails(ClassDetails) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'addClassDetails', ClassDetails);
  }

  getAllClassDetails(skip,limit) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allClassDetails'+'/'+skip+'/'+limit );
  }

  getClassDetailsByMongodbId(classId) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'classDetailsById/' + classId);
  }

  updateClassDetailsById(classId, classDetailsData) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'editClassDetails/' + classId, classDetailsData);
  }


  deleteClassDetailsByMongodbId(classId) {
    return this.http.delete(this.configService.appConfig.appBaseUrl + 'deleteClassDetails/' + classId);
  }

  getClassDetailsCount() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allClassDetails/count');
  }

}

