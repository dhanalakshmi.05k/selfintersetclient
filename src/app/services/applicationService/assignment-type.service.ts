import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ApiConfigService} from "../configservice/api-config.service";

@Injectable({
  providedIn: 'root'
})
export class AssignmentTypeService {

constructor(private http: HttpClient, public configService: ApiConfigService) {
  }

  saveAssignmentTypeDetails(assignmentTypeDetail) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'addAssignmentType', assignmentTypeDetail);
  }

  getAllAssignmentType(start,end) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentType/'+start
    +'/'+end );
  }

  getAssignmentTypeByMongodbId(assignmentTypeId) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'assignmentTypeById/' + assignmentTypeId);
  }

  updateAssignmentTypeById(assignmentTypeId, assignmentTypeData) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'editassignmentTypeById/' + assignmentTypeId, assignmentTypeData);
  }


  deleteAssignmentTypeByMongodbId(assignmentTypeId) {
    return this.http.delete(this.configService.appConfig.appBaseUrl + 'deleteAssignmentType/' + assignmentTypeId);
  }

  getAssignmentTypeCount() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentType/count');
  }

}
