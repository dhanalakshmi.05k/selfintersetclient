import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ApiConfigService} from "../configservice/api-config.service";

@Injectable({
  providedIn: 'root'
})
export class YearDetailsService {

constructor(private http: HttpClient, public configService: ApiConfigService) {
  }

  saveYearDetailsDetails(yearDetailsDetails) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'addYearDetails', yearDetailsDetails);
  }

  getAllYear(skip,limit) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allYear'+'/'+skip+'/'+ limit);
  }

  getYearByMongodbId(yearId) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'yearById/' + yearId);
  }

  updateYearById(yearId, yearData) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'editYearById/' + yearId, yearData);
  }


  deleteYearByMongodbId(yearId) {
    return this.http.delete(this.configService.appConfig.appBaseUrl + 'deleteYear/' + yearId);
  }

  getYearCount() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allYear/count');
  }

}
