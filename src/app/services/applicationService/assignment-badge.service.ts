import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ApiConfigService} from "../configservice/api-config.service";

@Injectable({
  providedIn: 'root'
})
export class AssignmentBadgeService {

  constructor(private http: HttpClient, public configService: ApiConfigService) {
  }

  saveAssignmentBadgeDetails(assignmentBadgeDetails) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'addAssignmentBadge', assignmentBadgeDetails);
  }

  getAllAssignmentBadge(start,end) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentBadge/'+ start+'/'+end);
  }

  getAssignmentBadgeByMongodbId(assignmentBadgeId) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'assignmentBadgeById/' + assignmentBadgeId);
  }

  updateAssignmentBadgeById(assignmentBadgeId, assignmentBadgeData) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'editAssignmentBadgeById/' + assignmentBadgeId, assignmentBadgeData);
  }


  deleteAssignmentBadgeByMongodbId(assignmentBadgeId) {
    return this.http.delete(this.configService.appConfig.appBaseUrl + 'deleteAssignmentBadge/' + assignmentBadgeId);
  }

  getAssignmentBadgeCount() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentBadge/count');
  }

}
