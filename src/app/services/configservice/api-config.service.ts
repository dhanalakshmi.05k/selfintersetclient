import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiConfigService {


  //http://51.15.95.69:5200/

  public appConfig = {
    appBaseUrl: 'http://localhost:5200/'
  };

  constructor() {
  }
}


