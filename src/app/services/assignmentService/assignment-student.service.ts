import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "../configuration/config.service";

@Injectable({
  providedIn: 'root'
})
export class AssignmentStudentService {

  constructor(private http: HttpClient, public configService: ConfigService) { }

  saveAssignmentToStudent(assignmentStudentData) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'addStudentToAssignment', assignmentStudentData);
  }

  getStudentsByAssignmentID(assignmentId) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'getAllStudentsPerAssignment/'+ assignmentId);
  }

  getStudentsByStudentEmailId(studentEmail) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'getStudentsByEmailId/'+ studentEmail);
  }
}
