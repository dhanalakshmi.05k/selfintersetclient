import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ApiConfigService} from "../configservice/api-config.service";


@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  constructor(private http: HttpClient, public configService: ApiConfigService) { }

  saveQuestionDetails(questiondata) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'addQuestions', questiondata);
  }

  getQuestionsByCount() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allQuestions/count' );
  }

  getAllQuestionDetails() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allQuestions' );
  }
  deleteQuestionByMongodbId(questionMongoId) {
    return this.http.delete(this.configService.appConfig.appBaseUrl + 'deleteQuestions/' + questionMongoId);
  }
  assignmentQuestionsByAssignmentId(assginmentId){
    return this.http.get(this.configService.appConfig.appBaseUrl + 'assignmentQuestionsByAssignmentId/'+ assginmentId);
  }
}
