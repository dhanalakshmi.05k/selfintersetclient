import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AssignmentServiceService} from "../../services/assignmentService/assignment-service.service";
import {QuestionService} from "../../services/assignmentService/question.service";
declare var $: any;
import * as _ from 'lodash';

@Component({
  selector: 'app-createassigmentaddquestion',
  templateUrl: './createassigmentaddquestion.component.html',
  styleUrls: ['./createassigmentaddquestion.component.css']
})
export class CreateassigmentaddquestionComponent implements OnInit {
  assignmentDeatilsParamsId:any;
  assignmentSavedDetails:any
  questionType:string="Text"
  questionIntermediateArr:any=[]
  textQuestionModelObj:any={}
  finalQuestionsArr:any=[]
  formChoiceOutput:any=[]
  formMatchOutput:any=[]
  formFillOutput:any=[]
  choiceObj:any={
    choiceTitle: "",
    choiceTypes: [],
    questionWeight:0

  }

  fillObj:any={
    fillTitle: "",
    fillOptionsList: [],
    fillQuestionWeight:0
  }

  totalAssignmentWeight:number=0
  textQuestionWeight:number
  isMatchDistractor=false
  matchObj:any={
    matchMainQuestion:"",
    matchOptionArr:[],
    matchQuestionWeight:0

  }
  isActiveRouteHighlight=false
  choiceOptionInterMediatIttr:number=0
  constructor(public router:Router, private assignmentServiceService:AssignmentServiceService,private questionService:QuestionService,
              private activeRoute: ActivatedRoute) {
    if(this.router.url.includes('/createQuestion')){
      this.isActiveRouteHighlight=true
    }
  }




  toggle(){
    console.log("here is toggle function goes")
  }

  fnCollapse(attribut){

    console.log("here>>>>commingg see here>>>>>>>fnCollapse")
    console.log(attribut)
    //let testclass = "demo1"
    //var IdTest = $(".demo1").attr('id');
    $("#"+ attribut +">.qBlockHolder").slideUp();
    $("#"+ attribut +">.listView").addClass("DB");
  }

  fnExpand(attribut){

    console.log("here>>>>>>>>>>>fnExpand")


    //var IdTest =  $(".qCont").find(".demo1").attr('id');
    $("#"+ attribut +">.qBlockHolder").show(600,"linear");
    $("#"+ attribut +">.listView").removeClass("DB");
  }


  fnCollapseAll(){
    console.log("fnCollapseAll")
    $(".demo1").find(".qBlockHolder").css("display","none");
    $(".demo1").find(".listView").addClass("DB");
  }

  fnClose(){

    $(".drag").off("click").on("click", function(){
      console.log("here>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    })

  }

  showQuestionType(typeName){

    console.log("**********this.choiceTypes*******")
    console.log(typeName)
    this.questionType=typeName
    if(typeName==='Text'){
      let questionObj={}
      questionObj["textQuestionTitle"+this.finalQuestionsArr.length]=""
      questionObj["textQuestionDesp"+this.finalQuestionsArr.length]=""
      questionObj["textQuestionWeight"+this.finalQuestionsArr.length]=0
      questionObj["questionType"+this.finalQuestionsArr.length]='Text'
      questionObj["index"+this.finalQuestionsArr.length]=this.finalQuestionsArr.length
      this.finalQuestionsArr.push(questionObj)
    }
    if(typeName==='Choice'){
      let choiceObj={}
      choiceObj["questionType"+this.finalQuestionsArr.length]='Choice'
      choiceObj["choiceTitle"+this.finalQuestionsArr.length]=""
      choiceObj["choiceQuestionWeight"+this.finalQuestionsArr.length]=0
      choiceObj['choiceTypes']=[]
      choiceObj["index"+this.finalQuestionsArr.length]=this.finalQuestionsArr.length
      this.finalQuestionsArr.push(choiceObj)

    }
    if(typeName==='Fill'){
      let fillObj={}
      fillObj["fillTitle"+this.finalQuestionsArr.length]=''
      fillObj["fillOptionsList"]=[]
      fillObj["fillQuestionWeight"+this.finalQuestionsArr.length]=0
      fillObj["questionType"+this.finalQuestionsArr.length]='Fill'
      this.finalQuestionsArr.push(fillObj)
    }
    if(this.questionType ==='Match'){
      let matchObj={}
      matchObj["matchMainQuestion"+this.finalQuestionsArr.length]=''
      matchObj["matchOptionsList"]=[]
      matchObj["matchQuestionWeight"+this.finalQuestionsArr.length]=0
      matchObj["questionType"+this.finalQuestionsArr.length]='Match'
      this.finalQuestionsArr.push(matchObj)

    }
  }

  moveToCreateGroup(){
    this.router.navigate(['createGroup']);
  }


  addMoreFillOption(questionObj,i) {
    let fillObj={}
    questionObj["fillOptionsList"].push(fillObj['modelValue']="value"+questionObj["fillOptionsList"].length)

  }


  addMoreChoiceOption(questionObj,index) {
    let inputObj={}
    inputObj['modelValue']="value"+questionObj['choiceTypes'].length
   questionObj['choiceTypes'].push(inputObj)
    console.log(">>>>>>>>>>>this.finalQuestionsArr>>>123123123>>>>>>>>>>>>")
    console.log(this.finalQuestionsArr)
  }



  addToIntermediateArray(){

    for(let h=0;h<this.finalQuestionsArr.length;h++){
      console.log('textQuestionWeight'+h)
      let constIndex='textQuestionWeight'+h
       this.totalAssignmentWeight=this.totalAssignmentWeight+parseInt(this.finalQuestionsArr[h][constIndex])
    }
    console.log("7777777777777this.this.totalAssignmentWeight " )
    console.log(this.totalAssignmentWeight)
  }


  markChoiceAsAnswer(choiceObject){
    console.log("this.choiceObject==================")
    console.log(choiceObject)
  }

  addMoreMatchOption(questionObj,i){
    let matchObject={}
    console.log("this.choiceObject========addMoreMatchOption==========")
    matchObject['leftquestion']="leftquestion"+questionObj['matchOptionsList'].length
    matchObject['rightAnswer']="rightAnswer"+questionObj['matchOptionsList'].length
    matchObject['isMatchDistractor']=false
    questionObj['matchOptionsList'].push(matchObject);
  }


  addMoreDistractorsOption(questionObj,i){
    console.log("this.choiceObject====distractor added==============")
    let matchObj={}
    matchObj['leftquestion']="leftquestion"+questionObj['matchOptionsList'].length
    matchObj['rightAnswer']="rightAnswer"+questionObj['matchOptionsList'].length
    matchObj['isMatchDistractor']=true
    questionObj['matchOptionsList'].push(matchObj);
  }

  ngOnInit() {
    const queryParams = this.activeRoute.snapshot.queryParams
    const routeParams = this.activeRoute.snapshot.params;
    console.log("999999999999990000000000000000")
    console.log(routeParams.id)
    this.assignmentDeatilsParamsId=routeParams.id
    this.getAssignmentDetails(routeParams.id);
  }


  getAssignmentDetails(assignmentId){
    this.assignmentServiceService.getAssignmentDetails(assignmentId)
      .subscribe((data: any) => {
        console.log("getting assignmentdetails?????")
        console.log(data)
        this.assignmentSavedDetails=data[0]
      });
  }

  submitAssignmetDetails(){

    let that=this
    var obj1={}
    obj1['assignmentId']=this.assignmentDeatilsParamsId
    obj1['AssignmentQuestion']=this.finalQuestionsArr
    obj1['finalAssignmentMarks']=this.totalAssignmentWeight
    console.log(obj1)
    that.questionService.saveQuestionDetails(obj1)
      .subscribe((data: any) => {
        that.assignmentServiceService.setAssginmentMongoDB(data)
        that.router.navigate(['createGroup',this.assignmentDeatilsParamsId]);

      });
  }

  send(){
    console.log("this.choiceObject====distractor added==============")
    this.router.navigate(['createGroup']);
  }
  deleteQuestion(questionObj){
    console.log("questionObj====questionObjndexOf(questionObj)==============")
    console.log(questionObj)
    console.log(this.finalQuestionsArr.indexOf(questionObj))
    this.finalQuestionsArr.splice(this.finalQuestionsArr.indexOf(questionObj),1)


  }
  naviagetToDashbord(){
    this.router.navigate(['mainDashBoard']);
  }

}
