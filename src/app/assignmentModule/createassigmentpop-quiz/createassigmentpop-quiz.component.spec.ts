import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateassigmentpopQuizComponent } from './createassigmentpop-quiz.component';

describe('CreateassigmentpopQuizComponent', () => {
  let component: CreateassigmentpopQuizComponent;
  let fixture: ComponentFixture<CreateassigmentpopQuizComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateassigmentpopQuizComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateassigmentpopQuizComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
