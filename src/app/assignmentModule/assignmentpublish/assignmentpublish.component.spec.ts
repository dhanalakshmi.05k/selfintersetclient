import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignmentpublishComponent } from './assignmentpublish.component';

describe('AssignmentpublishComponent', () => {
  let component: AssignmentpublishComponent;
  let fixture: ComponentFixture<AssignmentpublishComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignmentpublishComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentpublishComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
