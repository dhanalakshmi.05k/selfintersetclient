import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {StudentDetailsService} from "../../services/usermanagement/student-details.service";
import {AssignmentGroupService} from "../../services/assignmentService/assignment-group.service";
import {AssignmentServiceService} from "../../services/assignmentService/assignment-service.service";
import {AssignmentStudentService} from "../../services/assignmentService/assignment-student.service";
import {ConfigurationServiceService} from "../../services/configuration/configuration-service.service";
declare var $: any;
import * as jsPDF from 'jspdf';
import * as html2canvas from 'html2canvas';
import {AssignmentService} from "../../services/assignmentService/assignment.service";

@Component({
  selector: 'app-assignmentpublish',
  templateUrl: './assignmentpublish.component.html',
  styleUrls: ['./assignmentpublish.component.css']
})
export class AssignmentpublishComponent implements OnInit {
  public isNotifyOffline=false
  public isGroupPublish=false
  private studentList:any=[];
  private sendStudentEmailList:any=[]
  private showAssignmentStatusDetails:any=[]
  private GroupDetailsList:any=[]
  public assignmentParameterDetails:any={}
  public groupDetails:any
  public GroupIdList:any
  public questionDetailsMarks:number
  assignmentDeatilsParamsId:any;
  questionDetailsList:any;
  constructor(public router:Router,private assignmentStudentService:AssignmentStudentService
    ,private studentDetailsService:StudentDetailsService,private assignmentDetailsService :AssignmentService,
    private assignmentGroupService:AssignmentGroupService, private activeRoute: ActivatedRoute,
              private assignmentService:AssignmentServiceService,private configurationServiceService:ConfigurationServiceService) {

  }

  ngOnInit() {
    const queryParams = this.activeRoute.snapshot.queryParams
    const routeParams = this.activeRoute.snapshot.params;
    this.assignmentDeatilsParamsId=routeParams.id
    if(routeParams.id2=== "group"){
      this.isGroupPublish=true
       this.assignmentDeatilsParamsId=routeParams.id
    this.getAssignmentDetailsByParam(routeParams.id)

    }

   else if(routeParams.id2=== "perStudent"){
      this.isGroupPublish=false
      this.assignmentDeatilsParamsId=routeParams.id
      this.getAssignmentDetailsByParam(routeParams.id)

    }

  }



  generatePDF() {

    const div = document.getElementById("html2Pdf");
    const options = {background: "white", height: 1200, width: 1200};
    html2canvas(div, options).then((canvas) => {
      //Initialize JSPDF
      let doc = new jsPDF("l", "pt", "letter");

      //Converting canvas to Image
      let imgData = canvas.toDataURL("image/PNG");
      //Add image Canvas to PDF
      doc.addImage(imgData, 'PNG', 20, 20);

      let pdfOutput = doc.output();
      // using ArrayBuffer will allow you to put image inside PDF
      let buffer = new ArrayBuffer(pdfOutput.length);
      let array = new Uint8Array(buffer);
      for (let i = 0; i < pdfOutput.length; i++) {
        array[i] = pdfOutput.charCodeAt(i);
      }

      //Name of pdf
      const fileName = "AssignmentQuestions.pdf";

      // Make file
      doc.save(fileName);

    });



  }



  getAssignmentDetailsByParam(assignmentId){

    this.assignmentService.getAssignmentDetails(assignmentId)
      .subscribe((data: any) => {
        this.assignmentParameterDetails=data[0]
        this.showAssignmentStatusDetails=data[0]
        this.assignmentService.setAssginmentStudentDetails(data[0])


      });
  }


  getAssignmentQuestionByIDDetails(){
    let that=this
    that.assignmentService.getAssignmentQuestionDetails(that.assignmentDeatilsParamsId)
      .subscribe((data: any) => {
        console.log("?????????data?????========Question detils================???")
        console.log(data[0].AssignmentQuestion)
        that.questionDetailsMarks=data[0].finalAssignmentMarks
        that.questionDetailsList=data[0].AssignmentQuestion
        console.log(that.questionDetailsList)

      });
  }

  addPublishclass(){
    $("#publishStatus").addClass("on")
  }

  groupAssignmentDetails(grouplist){
    for(let m=0;m<grouplist;m++){
      this.assignmentGroupService.getAssignmentGroupId(grouplist[m])
        .subscribe((data: any) => {
          console.log(data)
      this.GroupDetailsList.push(data[0])
        })
    }
    console.log("this.GroupDetailsList>>>>>>")
    console.log(this.GroupDetailsList)
    this. formEmailList();
  }


  formEmailList(){
    for(var l=0;l<this.GroupDetailsList.length;l++){
      console.log("this.formEmailList>>>>>formEmailListformEmailList>")
      console.log(this.GroupDetailsList[l])
    }
  }


  getAllAssignmentGroupDetailsByAssignmentId(assignmentId){
    let publishStudentList:any=[]
    this.assignmentGroupService.getAssignmentGroupDetailsByAssignmentId(assignmentId)
      .subscribe((data: any) => {
        console.log("getting groupmdetails------questionsService-------");
        console.log(data);
        for(let l=0;l<data.length;l++){
          for(let m=0;m<data[l].studentDetails;m++){
            publishStudentList.push(data[l].studentDetails[m].studentName)
          }
        }
       this.sendEmailStudentList(publishStudentList)
      });
  }

  sendEmailStudentList(studentPublishList){

    console.log("Before sending email-------------");
    console.log(studentPublishList);

/*    this.studentDetailsService.notifyAssignmentDetailsToStudents(studentPublishList)
      .subscribe(response => {

      });*/
    /*this.router.navigate(['mainDashBoard'])*/
  }
  status: boolean = false;
  fnTOggle(){
    this.status = !this.status; 
  }



  getAllAssignmentGroupDetails(){
    let assignmentDetails=this.showAssignmentStatusDetails;

    let publishStudentList:any=[]
    let assGroupDetails:any=[]
    for(let r=0;r<assignmentDetails['assginmentGroupId'].length;r++){
      this.assignmentGroupService.getAssignmentGroupId(assignmentDetails['assginmentGroupId'][r])
        .subscribe((data: any) => {
          for(let m=0;m<data[0].studentDetails.length;m++){
            publishStudentList.push(data[0].studentDetails[m].studentEmail)
          }
          this.studentDetailsService.notifyAssignmentDetailsToStudents(publishStudentList)
            .subscribe(response => {
              console.log("email status--------")
              console.log(response)
            });
    })

      console.log("email status--------")
      console.log(assignmentDetails['assginmentGroupId'].length)
      console.log(r)
    /*  if(r==assignmentDetails['assginmentGroupId'].length-1){

      }*/
  }
    this.router.navigate(['mainDashBoard']);
    this.assignmentService.setAssginmentStatus(true)

}
  formStudentLsit(assGroupDetails){
    console.log("formStudentLsit----questionsService-------");
    console.log(assGroupDetails);
  }
 /* publishStudentAssign(){
    console.log("kdklsdjksjdf---studentstudent-----")
    console.log(this.groupDetails.studentDetails)
    /!*    let obj={}
        obj['studentEmail']=student['studentDetails']['studentEmail']
        obj['studentId']=student['_id']*!/
    this.studentList.push(this.groupDetails.studentDetails['studentEmail']);
    console.log("this.selectedStudentsForAssignment.8098rrrrrrrrrrrrrrrrstudentDetails44444444444rrrrr")
    console.log(this.studentList)
    this.publishEmailToStudents()
  }

  publishEmailToStudents(){
    console.log("this.selectedStudentsForA----------------------4444444444rrrrr")
    console.log(this.studentList)
 /!*   this.studentDetailsService.notifyAssignmentDetailsToStudents(this.studentList)
      .subscribe(response => {
        console.log("kdklsdjksjdf--------")
        console.log(response)
      });*!/
 this.assignmentService.setAssginmentStatus()
    this.router.navigate(['mainDashBoard']);

  }*/
  notififyOffline(){
    this.isNotifyOffline=true

  }
  publishToAllstudentsPerAssignment(){
   let publishIndStudentList:any=[]
    this.assignmentDetailsService.getAssignmentDetails(this.assignmentDeatilsParamsId)
      .subscribe((data: any) => {

        if(data && data.studentDetails)
        for(let m=0;m<data.studentDetails.length;m++){
          console.log("this.selectedStudentsForA----------------------4444444444rrrrr")
          console.log(data.studentDetails[m])
          publishIndStudentList.push(data.studentDetails[m]['studentEmail'])
          // for(let j=0;j<data[m]['studentDetails'].length;j++){
          //
          //
          // }
        }
        console.log("this.selectedStudentsForA----------------------4444444444rrrrr")
        console.log(publishIndStudentList)
        this.studentDetailsService.notifyAssignmentDetailsToStudents(publishIndStudentList)
          .subscribe(response => {
            console.log("email status--------")
            console.log(response)
          });
        this.router.navigate(['mainDashBoard']);
        this.assignmentService.setAssginmentStatus(true)
      });
  }


  moveToCreateGroup(){
    this.router.navigate(['createGroup']);
  }

  navigateToListAssignmentView(){
    console.log(this.assignmentDeatilsParamsId)
    this.router.navigate(['showAssignmentQuestions',this.assignmentDeatilsParamsId]);

  }

  getAllGrades(){
 /*   this.configurationServiceService.getAllGradingTools()
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentPublishDetails.gradesList=data;
      });*/
  }

}
