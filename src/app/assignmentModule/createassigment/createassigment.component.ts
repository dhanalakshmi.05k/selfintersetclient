import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ApplicationConfigurationService} from "../../services/configservice/application-configuration.service";
import {AssignmentServiceService} from "../../services/assignmentService/assignment-service.service";
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { FileUploader } from 'ng2-file-upload';
declare var $: any;



function readBase64(file): Promise<any> {
  var reader  = new FileReader();
  var future = new Promise((resolve, reject) => {
    reader.addEventListener("load", function () {
      console.log("resloeve seponsewwwwwwwwwwwwwwwww");
      console.log(reader.result)
      resolve(reader.result);
    }, false);

    reader.addEventListener("error", function (event) {
      reject(event);
    }, false);

    reader.readAsDataURL(file);
  });
  return future;
}

// const URL = '/api/';
const URL = 'http://51.15.95.69:5200/api/upload';



@Component({
  selector: 'app-createassigment',
  templateUrl: './createassigment.component.html',
  styleUrls: ['./createassigment.component.css']
})
export class CreateassigmentComponent implements OnInit {
  public isEditMode=false
  public assignmentEditParameterDetails:any={}
  public Editor = ClassicEditor;
  public editorValue: string = '';
  public currentFaculty:string;
  private fileNamesUploaded:any=[];
  public assignmentDespLengthError=""
  public assignmentMenuDetails:any={
    assignmentOption:{},
    levelOfProficiency:[],
    skillDetails:[],
    sectionList:[],
    splitBoardDetails:[],
    assignmentReason:[],
    gradingType:[],
    gradingTools:[],
    assignmentFocus:[],
    classDetails:[],
    dateError:"",
    markByLIst:[],
    assignFinalEvalutionType:['submitive','formitive']
  }

  public assignmentDropDownSettings={
    finalMarkBySettings: {
      singleSelection: false,
      idField: '_id',
      textField: 'assignmentEvaluationName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 1,
      allowSearchFilter: true,
      closeDropDownOnSelection:true
    },
    gradingToolsSettings: {
      singleSelection: false,
      idField: '_id',
      textField: 'gradeName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      closeDropDownOnSelection:true
    },
    ataSkillsSettings: {
      singleSelection: false,
      idField: '_id',
      textField: 'assignmentSkillDetailName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      closeDropDownOnSelection:true
    },
    gradeBySettings: {
      singleSelection: false,
      idField: '_id',
      textField: 'gradingName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      closeDropDownOnSelection:true
    },
    levelOfProficiencySettings: {
      singleSelection: false,
      idField: '_id',
      textField: 'assignmentLevelOfProficiencyName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      closeDropDownOnSelection:true
    }
  }

  /*onItemSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }
*/
  assignmentDetails:any={}
  constructor(public router:Router,private configurationServiceService:ApplicationConfigurationService,
              private assignmentServiceService:AssignmentServiceService,private activeRoute: ActivatedRoute) {
    console.log("global user ???????????????????")
    this.assignmentDetails={}
    console.log( JSON.parse(localStorage.getItem('currentUser')))
    this.currentFaculty=JSON.parse(localStorage.getItem('currentUser'))
  }






  submitAssignmetDetails(){
    if ((Date.parse(this.assignmentDetails.assignmentStartDate) >= Date.parse(this.assignmentDetails.assignmentEndDate))) {
     this.assignmentMenuDetails.dateError="Assigmnet End date should be greater than Start date";
      console.log(this.assignmentServiceService.getAssginmentOption())
      this.assignmentMenuDetails.assignmentOption=this.assignmentServiceService.getAssginmentOption()
    }

    else if(this.assignmentDetails && this.assignmentDetails.description.length===0){
    this.assignmentDespLengthError="please enter assignment description"
    }
    else{
      console.log(this.assignmentServiceService.getAssginmentOption())
      console.log(this.assignmentDetails);
      this.assignmentMenuDetails.assignmentOption=this.assignmentServiceService.getAssginmentOption()

      let saveObj = Object.assign(this.assignmentServiceService.getAssginmentOption(), this.assignmentDetails)
      saveObj['facultyName']=this.currentFaculty['user']
      saveObj['attachmentDetails']=this.fileNamesUploaded
      saveObj['ATAType']="Assignment"
      console.log("*fffffffffff")
      console.log(saveObj)
      this.assignmentServiceService.saveAssignmentDetails(saveObj)
        .subscribe((data: any) => {
          console.log("after assignment save");
          console.log(data);
          this.assignmentDespLengthError=""
          this.router.navigate(['createQuestion',data['_id']]);
        });
    }
  }

  navigateToDashboard(){
    this.router.navigate(['mainDashBoard']);
  }


  public uploader: FileUploader = new FileUploader({url: URL,itemAlias: 'file'});
  public hasBaseDropZoneOver:boolean = false;
  public hasAnotherDropZoneOver:boolean = false;

  fileObject: any;


  public fileOverBase(e:any):void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e:any):void {
    this.hasAnotherDropZoneOver = e;
  }

  ngOnInit() {
    this.getAllFinalMarkByAssignmentEvaluation();
    this.assignmentDetails={};
    const routeEditParams = this.activeRoute.snapshot.params;
    if(routeEditParams && routeEditParams.id){
      this.isEditMode=true
      this.getAssignmentDetails(routeEditParams.id)
    }

    this.getAllGradeType();
    this.getAllGradingTools();
    this.getAllSkillDetails();
    this.getAllSplitBoard();
    this.getAllLevelOfProficiency();
    this.getAllAssignmentFocus();
    this.assignmentMenuDetails.assignmentOption=this.assignmentServiceService.getAssginmentOption()
    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.fileNamesUploaded.push(JSON.parse(response)[0]. filename)
    };
  }


  getAllFinalMarkByAssignmentEvaluation(){
    this.configurationServiceService.getAllassignmentEvaluation()
      .subscribe((data: any) => {
        this.assignmentMenuDetails.markByList= data;
      });
  }
  getAllGradeType(){
    this.configurationServiceService.getAllGradingType()
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentMenuDetails.gradeType= data;
      });
  }
  getAllGradingTools(){
    this.configurationServiceService.getAllGradingTools()
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentMenuDetails.gradingTools= data;
      });
  }

  getAllLevelOfProficiency(){
    this.configurationServiceService.getAllAssignmentLevelOfProficiency()
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentMenuDetails.levelOfProficiency= data;
      });
  }
  getAllSkillDetails(){
    this.configurationServiceService.getAllAssignmentSkillDetail()
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentMenuDetails.skillDetails= data;
      });
  }
  getAllSplitBoard(){
    this.configurationServiceService.getAllAssignmentSplitBoard()
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentMenuDetails.splitBoardDetails= data;
      });
  }
  getAllAssignmentReason(){
    this.configurationServiceService.getAllAssignmentReason()
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentMenuDetails.assignmentReason= data;
      });
  }
  getAllGradingType(){
    this.configurationServiceService.getAllGradingType()
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentMenuDetails.gradingType= data;
      });
  }

  getAllAssignmentFocus(){
    this.configurationServiceService.getAllAssignmentSplitBoard()
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentMenuDetails.splitBoardDetails= data;
      });
  }
  getAssignmentDetails(assignmentId){
    this.assignmentServiceService.getAssignmentDetails(assignmentId)
      .subscribe((data: any) => {
        this.assignmentDetails=data[0]
      });
  }
  UpdateAssignmetDetails(){
    if ((Date.parse(this.assignmentDetails.assignmentStartDate) >= Date.parse(this.assignmentDetails.assignmentEndDate))) {
      this.assignmentMenuDetails.dateError="Assigmnet End date should be greater than Start date";
      console.log(this.assignmentServiceService.getAssginmentOption())
      this.assignmentMenuDetails.assignmentOption=this.assignmentServiceService.getAssginmentOption()
    }
    else{
      console.log("*fffffffffff")
      console.log(this.assignmentDetails)
      this.assignmentServiceService.updateAssignmentDetails(this.assignmentDetails)
        .subscribe((data: any) => {
          console.log(data);
          this.router.navigate(['assignmentListView']);
          this.assignmentMenuDetails.dateError=""
        });
    }
  }

}
