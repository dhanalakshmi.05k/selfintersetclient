import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateassigmentComponent } from './createassigment.component';

describe('CreateassigmentComponent', () => {
  let component: CreateassigmentComponent;
  let fixture: ComponentFixture<CreateassigmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateassigmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateassigmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
