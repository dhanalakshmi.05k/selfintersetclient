import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateassigmentgroupComponent } from './createassigmentgroup.component';

describe('CreateassigmentgroupComponent', () => {
  let component: CreateassigmentgroupComponent;
  let fixture: ComponentFixture<CreateassigmentgroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateassigmentgroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateassigmentgroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
