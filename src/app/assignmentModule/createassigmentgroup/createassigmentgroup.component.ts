import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {StudentDetailsService} from "../../services/usermanagement/student-details.service";
import {AssignmentGroupService} from "../../services/assignmentService/assignment-group.service";
import {AssignmentServiceService} from "../../services/assignmentService/assignment-service.service";
import {AssignmentStudentService} from "../../services/assignmentService/assignment-student.service";
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import * as _ from 'lodash';

declare var $: any;
@Component({
  selector: 'app-createassigmentgroup',
  templateUrl: './createassigmentgroup.component.html',
  styleUrls: ['./createassigmentgroup.component.css']
})
export class CreateassigmentgroupComponent implements OnInit {
  userLoginDetails:any;
  isTouched=false
  studentImportData:any;
  isGroupEnabled=false
  studentList:any=[]
  assignmentParameterDetails:any=[]
  assignmentParamsId:any;
  studentGroupList:any=[]
  selectedStudentListData:any=[]
  isActiveRouteHighlighted=false
  selectedStudentGroupListData:any=[]
  constructor(public router:Router
    ,private studentDetailsService:StudentDetailsService
    ,private assignmentStudentService:AssignmentStudentService
    ,private assignmentGroupService:AssignmentGroupService,private activeRoute: ActivatedRoute,
              private assignmentServiceService:AssignmentServiceService) {
    this.isGroupEnabled=false

    if(this.router.url.includes('/createGroup')){
      this.isActiveRouteHighlighted=true
    }
  }

  fnToggle(){
    $(".checkBtn").click(function(){
      $(".checkBtn").removeClass("active")
      $(this).addClass("active")
    })
  }



  studentListData:any;
  listIntermediateData:any;
  importStudentData(event){
    let that=this
    let tableObjs;
    let input = event   .target;
    let reader = new FileReader();
    reader.onload = function(){
      let fileData = reader.result;
      let wb = XLSX.read(fileData, {type : 'binary'});
      wb.SheetNames.forEach(function(sheetName){
        let rowObj =XLSX.utils.sheet_to_json(wb.Sheets[sheetName]);
        let tableObjs=JSON.stringify(rowObj);

        if (tableObjs == undefined || tableObjs== null){
          return;
        }
        else{
          /*console.log(JSON.stringify(rowObj))*/
          that.studentListData=JSON.stringify(rowObj)
          that.getIntermediate(that.studentListData)
        }
      })
    };
    reader.readAsBinaryString(input.files[0])
  }

  interStudentStateArr:any=[]
  getIntermediate(listData){
    let that=this
    let arrayListData = JSON.parse(listData);
    for(let g=0;g<arrayListData.length;g++){
      that.assignmentStudentService.getStudentsByStudentEmailId(arrayListData[g].studentEmail)
        .subscribe((data: any) => {
          if(data !==null){
            data['selected']=false
            this.interStudentStateArr.push(data)
          }

          if(g==arrayListData.length-1){
            this.studentList = this.studentList.concat(this.interStudentStateArr);
          }
        });

    }
  }


  intermediateArr:any=[]
  selectStudent(studentDetails){
    let studId=studentDetails['_id']
    let index = _.findIndex(this.intermediateArr, function(val) {
      return val['_id'] === studId;
    });

    if (index === -1) {
      this.changeFlag(studentDetails,true);
    } else {
      this.changeFlag(studentDetails,false);
    }

  }

  changeFlag(studentSelDetails,flag){
    for (let i=0;i<this.studentList.length;i++){
      if(studentSelDetails['_id'] == this.studentList[i]['_id']){
        this.studentList[i]['selected']= flag;
        if(this.studentList[i]['selected']){
          this.intermediateArr.push(this.studentList[i])
        }
        else if(!this.studentList[i]['selected']){
          let indexVal= this.intermediateArr.findIndex(vall=> vall['_id'] === studentSelDetails['_id']);
          if (indexVal > -1) {
            this.intermediateArr.splice(indexVal, 1);
          }
        }

      }
    }

    console.log("this.intermediateGroupArr???==========")
    console.log(this.intermediateArr)
  }



  intermediateGroupArr:any=[]
  selectIntermediateGroupDetails(groupDetails){
    let groupId=groupDetails['_id']

    let index = _.findIndex(this.intermediateGroupArr, function(val) {
      return val['_id'] === groupId;
    });

    if (index === -1) {
      this.changeGroupFlag(groupDetails,true);
    } else {
      this.changeGroupFlag(groupDetails,false);
    }

  }

  changeGroupFlag(groupSelDetails,flag){
      for (let m=0;m<this.studentGroupList.length;m++){
        if(groupSelDetails['_id'] == this.studentGroupList[m]['_id']){
          this.studentGroupList[m]['selected']= flag;
          if(this.studentGroupList[m]['selected']){
            this.intermediateGroupArr.push(this.studentGroupList[m])
          }
          else if(!this.studentList[m]['selected']){
            let indexVal= this.intermediateGroupArr.findIndex(vall=> vall['_id'] === groupSelDetails['_id']);
            if (indexVal > -1) {
              this.intermediateGroupArr.splice(indexVal, 1);
            }
          }
        }

      }

      console.log("this.intermediateGroupArr???==========")
    console.log(this.intermediateGroupArr)

  }



  enableStudent(){
    this.isGroupEnabled=false
  }

  ngOnInit() {

    this.getAllStudentsList()
    // this.getAllStudentsGroups()
    const queryParams = this.activeRoute.snapshot.queryParams
    const routeParams = this.activeRoute.snapshot.params;
    this.assignmentParamsId=routeParams.id
    this.getAssignmentDetails(this.assignmentParamsId)
    this.getUserDetailsBySession()

  }


  getUserDetailsBySession() {
    console.log("result????????????????????called????")
    if( JSON.parse(localStorage.getItem('currentUser')).user!=='admin@gmail.com'){
      this.assignmentServiceService.getUserDetailsSession()
        .subscribe((userDetails: any) => {
            this.userLoginDetails=userDetails
            console.log("userDetails>>>>>>>>>>>>>>>>>")

          },
          error => {
            // Handle result
            console.log("result????????????errorerror????????????")
            console.log(error)
          })
    }
    else{
      this.userLoginDetails="admin"
    }

  }


  getAssignmentDetails(assignmentId){
    this.assignmentServiceService.getAssignmentDetails(assignmentId)
      .subscribe((data: any) => {
        this.assignmentParameterDetails=data[0]
      });
  }


  moveToPublishAssignment(){
    this.router.navigate(['publishAssignment']);
  }

  getAllStudentsList(){
    this.isGroupEnabled=false
    this.studentDetailsService.getAllStudents()
      .subscribe((data: any) => {


        let interArr:any=[]
        for(let j=0;j<data.length;j++){

          data[j]['selected']=false
          interArr.push(data[j])
        }
        this.studentList= interArr
        console.log(this.studentList);
      });
  }

  enableStudentGroup(){
    this.isGroupEnabled=true
  }


  getAllStudentsGroups(){


    this.assignmentGroupService.getAllAssignmentGroupDetails()
      .subscribe((data: any) => {
        console.log("data>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>group name");
        console.log(data);
        let interGroupArr:any=[]
        for(let j=0;j<data.length;j++){

          data[j]['indexValue']=j
          data[j]['selected']=false
          interGroupArr.push(data[j])
        }
        this.studentGroupList= interGroupArr
      });
  }

  saveSelectedGroupToAssginment(){
    let groupIds=[]
    let studentGroupDetails=[]
    for(let k=0;k<this.intermediateGroupArr.length;k++){
      groupIds.push(this.intermediateGroupArr[k]['_id'])
      for(let r=0;r<this.intermediateGroupArr[k]['studentDetails'].length;r++){
        studentGroupDetails.push(this.intermediateGroupArr[k]['studentDetails'][r])
      }
    }
    this.assignmentParameterDetails['assginmentGroupId']=groupIds
    this.assignmentParameterDetails['studentDetails']=studentGroupDetails
    this.assignmentServiceService.updateAssignmentDetails(this.assignmentParameterDetails)
      .subscribe((data: any) => {
        this.router.navigate(['publishAssignment', this.assignmentParamsId,  "group"])
      });

  }

  saveSelectedStudentsToAssginment(){
    let obj={}
    obj['studentDetails']=this.intermediateArr
    obj['assignmentId']=this.assignmentParamsId
      this.assignmentStudentService.saveAssignmentToStudent(obj)
        .subscribe((data: any) => {
          this.router.navigate(['publishAssignment', this.assignmentParamsId,  "perStudent"])
        });

  }


}
