import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AssignmentServiceService} from "../../services/assignmentService/assignment-service.service";
import * as html2canvas from "html2canvas";
import * as jsPDF from "jspdf";
declare var $: any;
@Component({
  selector: 'app-assigment-list-view',
  templateUrl: './assigment-list-view.component.html',
  styleUrls: ['./assigment-list-view.component.css']
})
export class AssigmentListViewComponent implements OnInit {
  userLoginDetails:any;
  selectedDevice="Assignment"
  recordAvailableStatus=false
  assignmentToDelete:any={}
  deleteAssignmentIntermediate:any={}
  currentUser:any
  isListViewActive:boolean=false
  isGridViewActive:boolean=false
  localIndex:number
  assignmentList:any=[]
  constructor(private router: Router,private assignmentServiceService:AssignmentServiceService) {
    console.log("global user ???????????????????")
    console.log( JSON.parse(localStorage.getItem('currentUser')))
    this.currentUser=JSON.parse(localStorage.getItem('currentUser')).user
  }

  zoomAdded(assignmentDetails){
    console.log(assignmentDetails)
    this.localIndex=assignmentDetails.index
    $(".ListViewHolder").click(function(){
      $(".ListViewHolder").removeClass("selected")
      $(this).addClass("selected")
    })
   // this.navigateToEditAssignment(assignmentDetails)
  }

  popShowfn(){
    // $(".edit").click(function(){
       
    // }
  }

  showPopup(){
    $("#createOptionPopUp").removeClass("DN")
    $("#createOptionPopUp").addClass("DB")
  }

  ngOnInit() {
    this.assignmentServiceService.setAssginmentStatus(false)
    this.getAllAssignments(0,10);
    this.getUserDetailsBySession();
  }


  getUserDetailsBySession() {
    console.log("result????????????????????called????")
    if( JSON.parse(localStorage.getItem('currentUser')).user!=='admin@gmail.com'){
      this.assignmentServiceService.getUserDetailsSession()
        .subscribe((userDetails: any) => {
            this.userLoginDetails=userDetails
            console.log("userDetails>>>>>>>>>>>>>>>>>")

          },
          error => {
            // Handle result
            console.log("result????????????errorerror????????????")
            console.log(error)
          })
    }
    else{
      this.userLoginDetails="admin"
    }

  }

  logout(){
    this.router.navigate(['login']);
    localStorage.clear();
  }

  getAllAssignments(skip,limit){
    this.assignmentServiceService.getAllAssignmentDetailsBYFacultyName(this.currentUser)
      .subscribe((data: any) => {
        for(var j=0;j<data.length;j++){
          data[j]['index']=j
        }
        console.log(data);
        this.assignmentList= data.reverse();
      });
  }
  showRecordsBasedonType(recordType){
    console.log("************clicekd called"+recordType)
    if(recordType==="all"||recordType==="Assignment"){
      this.recordAvailableStatus=true
      this.getAllAssignments(0,10);
    }
    else{
      this.assignmentList=[]
      this.recordAvailableStatus=false
    }

  }



  navigateListView(){
    this.isGridViewActive=false
    this.isListViewActive=true
    this.router.navigate(['assignmentListView']);
  }
  navigateAssignment(){
    this.router.navigate(['mainDashBoard']);
  }
  navigateDashBoardView(){
    this.isGridViewActive=true
    this.isListViewActive=false
    this.router.navigate(['mainDashBoard']);
  }


  navigateToDeleteAssignment(assignmnentDetails){
this.assignmentToDelete=assignmnentDetails
  }

  navigateToEditAssignment(assignmnentEditDetails){
    console.log(assignmnentEditDetails)
    this.router.navigate(['createAssignment',assignmnentEditDetails['_id']]);

  }

  deleteAssignmentIntermediateDetails(assign){
    this.deleteAssignmentIntermediate=assign
  }

  deleteAssignmentDetails(){
    this.assignmentServiceService.deleteAssignmentDetails(this.deleteAssignmentIntermediate['_id'])
      .subscribe((data: any) => {
        console.log(data);
        this.getAllAssignments(0,10)
      });
  }

  generatePDF() {

    const div = document.getElementById("html2Pdf");
    const options = {background: "white", height: 900, width: 900};
    html2canvas(div, options).then((canvas) => {
      //Initialize JSPDF
      let doc = new jsPDF("l", "pt", "letter");

      //Converting canvas to Image
      let imgData = canvas.toDataURL("image/PNG");
      //Add image Canvas to PDF
      doc.addImage(imgData, 'PNG', 20, 20);

      let pdfOutput = doc.output();
      // using ArrayBuffer will allow you to put image inside PDF
      let buffer = new ArrayBuffer(pdfOutput.length);
      let array = new Uint8Array(buffer);
      for (let i = 0; i < pdfOutput.length; i++) {
        array[i] = pdfOutput.charCodeAt(i);
      }

      //Name of pdf
      const fileName = "Questions.pdf";

      // Make file
      doc.save(fileName);

    });



  }


}
