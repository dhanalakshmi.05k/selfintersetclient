import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TargertLearningComponent } from './targert-learning.component';

describe('TargertLearningComponent', () => {
  let component: TargertLearningComponent;
  let fixture: ComponentFixture<TargertLearningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TargertLearningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TargertLearningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
