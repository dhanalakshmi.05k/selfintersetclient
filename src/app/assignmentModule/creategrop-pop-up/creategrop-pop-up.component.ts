import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {StudentDetailsService} from "../../services/usermanagement/student-details.service";
import {AssignmentGroupService} from "../../services/applicationService/assignment-group.service";

@Component({
  selector: 'app-creategrop-pop-up',
  templateUrl: './creategrop-pop-up.component.html',
  styleUrls: ['./creategrop-pop-up.component.css']
})
export class CreategropPopUpComponent implements OnInit {
  @Input() selectedStudentList: any;
  @Input() assignmentSavedId: any;
  mainGroupName:string
  mainGroupEmail:string
  studentList:any=[]
  constructor(public router:Router,private studentDetailsService:StudentDetailsService,
              private assignmentGroupService:AssignmentGroupService) {

  }

  ngOnInit() {

  }
  saveAssginmentGroupDetails(){
    var obj={}
    obj['groupName']=this.mainGroupName
    obj['groupEmail']=this.mainGroupEmail
    obj['studentDetails']=this.selectedStudentList
    obj['assignmentId']=this.assignmentSavedId
    this.assignmentGroupService.saveAssignmentGroupDetails(obj)
      .subscribe((data: any) => {
        console.log("data?????????????????????????????????????? group cretaeed");
        console.log(data);



     /*   this.router.navigate(['publishAssignment',data['_id']]);*/


      });
  }

}
