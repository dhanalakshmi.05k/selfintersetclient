import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AssignmentServiceService} from "../../services/assignmentService/assignment-service.service";
import * as html2canvas from "html2canvas";
import * as jsPDF from "jspdf";



@Component({
  selector: 'app-questions-list-view',
  templateUrl: './questions-list-view.component.html',
  styleUrls: ['./questions-list-view.component.css']
})
export class QuestionsListViewComponent implements OnInit {

  questionDetailsList:any={}
  assignmentSavedDetails:any={}
  constructor(public router:Router,private activeRoute: ActivatedRoute,private assignmentServiceService:AssignmentServiceService) { }

  ngOnInit() {
    const routeEditParams = this.activeRoute.snapshot.params;
    if(routeEditParams && routeEditParams.id){

      this.getAssignmentDetails(routeEditParams.id)
      this.getAssignmentQuestionDetails(routeEditParams.id)
    }
  }

  getAssignmentQuestionDetails(assignmentId){
    let that=this
    that.assignmentServiceService.getAssignmentQuestionDetails(assignmentId)
      .subscribe((data: any) => {
       console.log("?????????data?????========================???")
        console.log(data[0].AssignmentQuestion)
        that.questionDetailsList=data[0].AssignmentQuestion
        console.log(that.questionDetailsList)

      });
  }


  getAssignmentDetails(assignmentId){
    this.assignmentServiceService.getAssignmentDetails(assignmentId)
      .subscribe((data: any) => {
        console.log("getting assignmentdetails?????")
        console.log(data)
        this.assignmentSavedDetails=data[0]
      });
  }

  naviagetToDashbord(){
    this.router.navigate(['mainDashBoard']);
  }



}
