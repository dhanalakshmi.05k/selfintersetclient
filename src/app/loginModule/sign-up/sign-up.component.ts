import { Component, OnInit } from '@angular/core';
import {UserManagementService} from "../../services/usermanagement/user-management.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  userCredentials:any={}
  repeatPasswordError=""
  constructor(private userManagementService:UserManagementService,private router: Router) { }

  ngOnInit() {
  }


  saveAssignmentDetails(){
    console.log(this.userCredentials)

    if(this.userCredentials.password===this.userCredentials.repeatPassword){
      this.userManagementService.saveUserDetails(this.userCredentials)
        .subscribe((data: any) => {
          console.log(data);
          this.router.navigate(['login']);
        });
    }
    else{
      this.repeatPasswordError="password mismatch"
    }

  }


}
