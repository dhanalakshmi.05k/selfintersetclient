import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignmentFinalMarkByComponent } from './assignment-final-mark-by.component';

describe('AssignmentFinalMarkByComponent', () => {
  let component: AssignmentFinalMarkByComponent;
  let fixture: ComponentFixture<AssignmentFinalMarkByComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignmentFinalMarkByComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentFinalMarkByComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
