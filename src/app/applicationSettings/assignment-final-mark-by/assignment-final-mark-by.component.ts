import { Component, OnInit } from '@angular/core';
import {AssignmentCategoryService} from "../../services/applicationService/assignment-category.service";

@Component({
  selector: 'app-assignment-final-mark-by',
  templateUrl: './assignment-final-mark-by.component.html',
  styleUrls: ['./assignment-final-mark-by.component.css']
})
export class AssignmentFinalMarkByComponent implements OnInit {

  assignmentCategoryObj:any={}
  assignmentCategoryList:any=[]
  isEdit:boolean=false
  deletebadgeObj:any={}

  constructor(public assignmentCategoryService:AssignmentCategoryService) { }

  ngOnInit() {
    this.getAssignmentCategoryDetails(0,10);
  }
  navigateToCreateAssignmentCategoryPopup(){
    this.isEdit=false
    $("#createAssignmentCategory").addClass("DB")
  }


  fnHidePop(){
    $("#createAssignmentCategory").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }

  fnDeleteHidePop(){
    $("#deleteAssignmentCategory").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }
  saveAssignmentCategory(){
    this.assignmentCategoryService.saveAssignmentCategoryDetails(this.assignmentCategoryObj)
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentCategoryList.unshift(data)
        this.hidePopup()
      });
  }

  getAssignmentCategoryDetails(skip,limit){
    this.assignmentCategoryService.getAllAssignmentCategory(skip,limit)
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentCategoryList=data.reverse()
      });
  }

  editIntermediatePopUp(categoryObj){
    this.isEdit=true
    this.assignmentCategoryObj=categoryObj
    this.EditOpenPopup()
  }

  updateAssignmentBadge(){
    this.assignmentCategoryService.updateAssignmentCategoryById(this.assignmentCategoryObj['_id'],this.assignmentCategoryObj)
      .subscribe((data: any) => {
        console.log(data);
        this.hidePopup()
      });
  }

  deleteIntermediatePopUp(categoryObj){
    this.deletebadgeObj=categoryObj
    this.openDeletePopup()
  }

  deleteAssignmentCategory(categoryObj){
    this.assignmentCategoryService.deleteAssignmentCategoryByMongodbId(categoryObj  ['_id'])
      .subscribe((data: any) => {
        console.log(data);
        this.getAssignmentCategoryDetails(0,10);
        this.closeDeletePopup()
      });
  }
  EditOpenPopup(){

    $("#createAssignmentCategory").removeClass("DN")
    $("#createAssignmentCategory").addClass("DB")
  }

  openPopup(){
    this.isEdit=false
    this.assignmentCategoryObj={}
    $("#createAssignmentCategory").removeClass("DN")
    $("#createAssignmentCategory").addClass("DB")
  }
  hidePopup(){
    $("#createAssignmentCategory").addClass("DN")
    $("#createAssignmentCategory").removeClass("DB")
    this.getAssignmentCategoryDetails(0,10);
  }
  openDeletePopup() {

    $("#deleteAssignmentCategory").removeClass("DN")
    $("#deleteAssignmentCategory").addClass("DB")
  }

  closeDeletePopup() {

    $("#deleteAssignmentCategory").addClass("DN")
    $("#deleteAssignmentCategory").removeClass("DB")
  }

}
