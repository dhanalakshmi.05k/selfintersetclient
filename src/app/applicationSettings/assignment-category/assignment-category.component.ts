import { Component, OnInit } from '@angular/core';
import {AssignmentBadgeService} from "../../services/applicationService/assignment-badge.service";
import {AssignmentCategoryService} from "../../services/applicationService/assignment-category.service";
import {FinalEvaluteTypeByService} from "../../services/applicationService/final-evalute-type-by.service";
declare var $: any;

@Component({
  selector: 'app-assignment-category',
  templateUrl: './assignment-category.component.html',
  styleUrls: ['./assignment-category.component.css']
})
export class AssignmentCategoryComponent implements OnInit {

  assignmentCategoryObj:any={}
  assignmentCategoryList:any=[]
  isEdit:boolean=false
  deletebadgeObj:any={}

  constructor(public finalEvaluteService:FinalEvaluteTypeByService) { }

  ngOnInit() {
    this.getAssignmentEvaluationType(0,10);
  }
  navigateToCreateAssignmentCategoryPopup(){
    this.isEdit=false
    $("#createAssignmentCategory").addClass("DB")
  }


  fnHidePop(){
    $("#createAssignmentCategory").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }

  fnDeleteHidePop(){
    $("#deleteAssignmentCategory").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }
  saveAssignmentCategory(){
    this.finalEvaluteService.saveEvalutionFinalMarkByDetails(this.assignmentCategoryObj)
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentCategoryList.unshift(data)
        this.hidePopup()
      });
  }

  getAssignmentEvaluationType(skip,limit){
    this.finalEvaluteService.getAllAssignmentEvalutionFinalMarkBy(skip,limit)
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentCategoryList=data.reverse()
      });
  }

  editIntermediatePopUp(categoryObj){
    this.isEdit=true
    this.assignmentCategoryObj=categoryObj
    this.EditOpenPopup()
  }
  updateAssignmentBadge(){
    this.finalEvaluteService.updateAssignmentEvalutionFinalMarkById(this.assignmentCategoryObj['_id'],this.assignmentCategoryObj)
      .subscribe((data: any) => {
        console.log(data);
        this.hidePopup()
      });
  }
  deleteIntermediatePopUp(categoryObj){
    this.deletebadgeObj=categoryObj
   this.openDeletePopup()
  }
  deleteAssignmentCategory(categoryObj){
    this.finalEvaluteService.deleteAssignmentEvalutionFinalMarkByMongodbId(categoryObj  ['_id'])
      .subscribe((data: any) => {
        console.log(data);
        this.getAssignmentEvaluationType(0,10);
        this.closeDeletePopup()
      });
  }
  EditOpenPopup(){

    $("#createAssignmentCategory").removeClass("DN")
    $("#createAssignmentCategory").addClass("DB")
  }
  openPopup(){
    this.isEdit=false
    this.assignmentCategoryObj={}
    $("#createAssignmentCategory").removeClass("DN")
    $("#createAssignmentCategory").addClass("DB")
  }
  hidePopup(){
    $("#createAssignmentCategory").addClass("DN")
    $("#createAssignmentCategory").removeClass("DB")
    this.getAssignmentEvaluationType(0,10);
  }
  openDeletePopup() {

    $("#deleteAssignmentCategory").removeClass("DN")
    $("#deleteAssignmentCategory").addClass("DB")
  }
  closeDeletePopup() {

    $("#deleteAssignmentCategory").addClass("DN")
    $("#deleteAssignmentCategory").removeClass("DB")
  }


}
