import { Component, OnInit } from '@angular/core';
import {AssignmentBadgeService} from "../../services/applicationService/assignment-badge.service";
import {SemisterDetailsService} from "../../services/applicationService/semister-details.service";
declare var $: any;
@Component({
  selector: 'app-semester-details',
  templateUrl: './semester-details.component.html',
  styleUrls: ['./semester-details.component.css']
})
export class SemesterDetailsComponent implements OnInit {

  assignmentBadgeObj:any={}
  assignmentEditBadgeObj:any={}
  assignmentSemesterList:any=[]
  isEdit:boolean=false
  deletebadgeObj:any={}

  constructor(public semisterDetailsService:SemisterDetailsService) { }

  ngOnInit() {
    this.getAssignmentBadgeDetails(0,10);

  }
  navigateToCreateAssignmentBadgePopup(){
    this.isEdit=false
    $("#createAssignmentSemsetser").addClass("DB")
  }


  fnHidePop(){
    $("#createAssignmentSemsetser").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }

  fnDeleteHidePop(){
    $("#deleteAssignmentSemester").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }
  saveAssignmentBadge(){

    this.semisterDetailsService.saveSemisterDetailsDetails(this.assignmentBadgeObj)
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentSemesterList.unshift(data)
        this.hidePopup()
      });
  }

  getAssignmentBadgeDetails(skip ,limit){
    this.semisterDetailsService.getAllSemister(skip,limit)
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentSemesterList=data.reverse()
      });
  }

  editIntermediatePopUp(badgeObj){
    this.isEdit=true
    this.assignmentEditBadgeObj=badgeObj
    this.EditDataOpenPopup()
  }

  EditDataOpenPopup(){

    $("#assignmentEditPopup").removeClass("DN")
    $("#assignmentEditPopup").addClass("DB")
  }



  updateAssignmentBadge(){
    this.semisterDetailsService.updateSemisterById(this.assignmentEditBadgeObj['_id'],this.assignmentEditBadgeObj)
      .subscribe((data: any) => {
        this.getAssignmentBadgeDetails(0,10);
         this.hideDataPopup()
      });
  }
  hideDataPopup(){
    this.getAssignmentBadgeDetails(0,10);
    $("#assignmentEditPopup").addClass("DN")
    $("#assignmentEditPopup").removeClass("DB")
  }

  deleteIntermediatePopUp(badgeObj){
    this.deletebadgeObj=badgeObj
     this.openDeletePopup()
  }

  deleteAssignmentBadge(badgeObj){
    this.semisterDetailsService.deleteSemisterByMongodbId(badgeObj['_id'])
      .subscribe((data: any) => {
        console.log(data);
        this.getAssignmentBadgeDetails(0,10);
        this.closeDeletePopup()
      });
  }
EditOpenPopup(){

    $("#createAssignmentSemsetser").removeClass("DN")
    $("#createAssignmentSemsetser").addClass("DB")
  }

  openPopup(){
    this.isEdit=false
    this.assignmentBadgeObj={}
    $("#createAssignmentSemsetser").removeClass("DN")
    $("#createAssignmentSemsetser").addClass("DB")
  }
  hidePopup(){
    $("#createAssignmentSemsetser").addClass("DN")
    $("#createAssignmentSemsetser").removeClass("DB")
    this.getAssignmentBadgeDetails(0,10);
  }
  openDeletePopup() {

    $("#deleteAssignmentSemester").removeClass("DN")
    $("#deleteAssignmentSemester").addClass("DB")
  }

  closeDeletePopup() {

    $("#deleteAssignmentSemester").addClass("DN")
    $("#deleteAssignmentSemester").removeClass("DB")
  }



}
