import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherBadgeComponent } from './teacher-badge.component';

describe('TeacherBadgeComponent', () => {
  let component: TeacherBadgeComponent;
  let fixture: ComponentFixture<TeacherBadgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherBadgeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherBadgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
