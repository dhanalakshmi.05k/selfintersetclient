import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HabitsOfLearnerComponent } from './habits-of-learner.component';

describe('HabitsOfLearnerComponent', () => {
  let component: HabitsOfLearnerComponent;
  let fixture: ComponentFixture<HabitsOfLearnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HabitsOfLearnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HabitsOfLearnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
