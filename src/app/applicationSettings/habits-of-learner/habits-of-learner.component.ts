import { Component, OnInit } from '@angular/core';
import {HabitsOfLearnerService} from "../../services/applicationService/habits-of-learner.service";
declare var $: any;

@Component({
  selector: 'app-habits-of-learner',
  templateUrl: './habits-of-learner.component.html',
  styleUrls: ['./habits-of-learner.component.css']
})
export class HabitsOfLearnerComponent implements OnInit {

habitsOfLearnerObj:any={}
habitsOfLearnerList:any=[]
isEdit:boolean=false
deletehabitsoflearnerObj:any={}

constructor(public habitsOfLearnerService:HabitsOfLearnerService) { }

  ngOnInit() {
    this.getHabitsOfLearnerDetails(0,10);

  }
  navigateToCreateHabitsOfLearnerPopup(){
    this.isEdit=false
    $("#createHabitsOfLearner").addClass("DB")
  }


  fnHidePop(){
    $("#createHabitsOfLearner").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }

  fnDeleteHidePop(){
    $("#deleteHabitsOfLearner").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }
  saveHabitsOfLearner(){
    this.habitsOfLearnerService.saveHabitsOfLearnerDetails(this.habitsOfLearnerObj)
      .subscribe((data: any) => {
        console.log(data);
        this.habitsOfLearnerList.unshift(data)
        this.hidePopup()
      });
  }

  getHabitsOfLearnerDetails(skip ,limit){
    this.habitsOfLearnerService.getAllHabitsOfLearnerDetails(skip,limit)
      .subscribe((data: any) => {
        console.log(data);
        this.habitsOfLearnerList=data.reverse()
      });
  }

  editIntermediatePopUp(habitsoflearnerObj){
   this.EditOpenPopup()
    this.isEdit=true
    this.habitsOfLearnerObj=habitsoflearnerObj

  }

  updateHabitsOfLearner(){
    this.habitsOfLearnerService.updateHabitsOfLearnerDetailsById(this.habitsOfLearnerObj['_id'],this.habitsOfLearnerObj)
      .subscribe((data: any) => {
        console.log(data);
        // this.getHabitsOfLearnerDetails(0,10);
        this.hidePopup()
      });
  }

  deleteIntermediatePopUp(habitsoflearnerObj){
    this.deletehabitsoflearnerObj=habitsoflearnerObj
     this.openDeletePopup()
  }

  deleteHabitsOfLearner(habitsoflearnerObj){
    this.habitsOfLearnerService.deleteHabitsOfLearnerDetailsByMongodbId(habitsoflearnerObj['_id'])
      .subscribe((data: any) => {
        console.log(data);
        this.getHabitsOfLearnerDetails(0,10);
        this.closeDeletePopup()
      });
  }
  EditOpenPopup(){

    $("#createHabitsOfLearner").removeClass("DN")
    $("#createHabitsOfLearner").addClass("DB")
  }

  openPopup(){
  this.isEdit=false
  this.habitsOfLearnerObj={}
    $("#createHabitsOfLearner").removeClass("DN")
    $("#createHabitsOfLearner").addClass("DB")
  }
  hidePopup(){
    $("#createHabitsOfLearner").addClass("DN")
    $("#createHabitsOfLearner").removeClass("DB")
    this.getHabitsOfLearnerDetails(0,10);
  }
  openDeletePopup() {

    $("#deleteHabitsOfLearner").removeClass("DN")
    $("#deleteHabitsOfLearner").addClass("DB")
  }

  closeDeletePopup() {

    $("#deleteHabitsOfLearner").addClass("DN")
    $("#deleteHabitsOfLearner").removeClass("DB")
  }
}

