import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignmentWeightComponent } from './assignment-weight.component';

describe('AssignmentWeightComponent', () => {
  let component: AssignmentWeightComponent;
  let fixture: ComponentFixture<AssignmentWeightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignmentWeightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentWeightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
