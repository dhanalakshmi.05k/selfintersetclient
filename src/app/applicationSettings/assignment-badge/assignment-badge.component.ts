import { Component, OnInit } from '@angular/core';
import {AssignmentBadgeService} from "../../services/applicationService/assignment-badge.service";
declare var $: any;

@Component({
  selector: 'app-assignment-badge',
  templateUrl: './assignment-badge.component.html',
  styleUrls: ['./assignment-badge.component.css']
})
export class AssignmentBadgeComponent implements OnInit {
  assignmentBadgeObj:any={}
  assignmentBadgeList:any=[]
  isEdit:boolean=false
  deletebadgeObj:any={}

  constructor(public assignmentBadgeService:AssignmentBadgeService) { }

  ngOnInit() {
    this.getAssignmentBadgeDetails(0,10);

  }
  navigateToCreateAssignmentBadgePopup(){
    this.isEdit=false
    $("#createAssignmentBadge").addClass("DB")
  }


  fnHidePop(){
    $("#createAssignmentBadge").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }

  fnDeleteHidePop(){
    $("#deleteAssignmentBadge").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }
  saveAssignmentBadge(){



    this.assignmentBadgeService.saveAssignmentBadgeDetails(this.assignmentBadgeObj)
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentBadgeList.unshift(data)
        this.hidePopup()
      });
  }

  getAssignmentBadgeDetails(skip ,limit){
    this.assignmentBadgeService.getAllAssignmentBadge(skip,limit)
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentBadgeList=data.reverse()
      });
  }

  editIntermediatePopUp(badgeObj){
    this.isEdit=true
    this.assignmentBadgeObj=badgeObj
    this.EditOpenPopup()
  }

  updateAssignmentBadge(){
    this.assignmentBadgeService.updateAssignmentBadgeById(this.assignmentBadgeObj['_id'],this.assignmentBadgeObj)
      .subscribe((data: any) => {
        console.log(data);
        this.hidePopup()
      });
  }

  deleteIntermediatePopUp(badgeObj){
    this.deletebadgeObj=badgeObj
    this.openDeletePopup()
  }

  deleteAssignmentBadge(badgeObj){
    this.assignmentBadgeService.deleteAssignmentBadgeByMongodbId(badgeObj['_id'])
      .subscribe((data: any) => {
        console.log(data);
        this.getAssignmentBadgeDetails(0,10);
        this.closeDeletePopup()
      });
  }

 EditOpenPopup(){

    $("#createAssignmentBadge").removeClass("DN")
    $("#createAssignmentBadge").addClass("DB")
  }

  openPopup(){
    this.isEdit=false
    this.assignmentBadgeObj={}
    $("#createAssignmentBadge").removeClass("DN")
    $("#createAssignmentBadge").addClass("DB")
  }
  hidePopup(){
    $("#createAssignmentBadge").addClass("DN")
    $("#createAssignmentBadge").removeClass("DB")
    this.getAssignmentBadgeDetails(0,10);
  }
  openDeletePopup() {

    $("#deleteAssignmentBadge").removeClass("DN")
    $("#deleteAssignmentBadge").addClass("DB")
  }

  closeDeletePopup() {

    $("#deleteAssignmentBadge").addClass("DN")
    $("#deleteAssignmentBadge").removeClass("DB")
  }



}
