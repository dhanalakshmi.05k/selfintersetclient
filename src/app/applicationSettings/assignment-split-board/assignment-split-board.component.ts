import { Component, OnInit } from '@angular/core';
import {AssignmentSplitBoardService} from "../../services/applicationService/assignment-split-board.service";
declare var $: any;

@Component({
  selector: 'app-assignment-split-board',
  templateUrl: './assignment-split-board.component.html',
  styleUrls: ['./assignment-split-board.component.css']
})
export class AssignmentSplitBoardComponent implements OnInit {

assignmentSplitBoardObj:any={}
assignmentSplitBoardList:any=[]
isEdit:boolean=false
deletesplitboardObj:any={}

constructor(public assignmentSplitBoardService:AssignmentSplitBoardService) { }

  ngOnInit() {
    this.getAssignmentSplitBoardDetails(0,10);

  }
  navigateToCreateAssignmentSplitBoardPopup(){
    this.isEdit=false
    $("#createAssignmentSplitBoard").addClass("DB")
  }


  fnHidePop(){
    $("#createAssignmentSplitBoard").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }

  fnDeleteHidePop(){
    $("#deleteAssignmentSplitBoard").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }
  saveAssignmentSplitBoard(){
    this.assignmentSplitBoardService.saveAssignmentSplitBoardDetails(this.assignmentSplitBoardObj)
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentSplitBoardList.unshift(data)
        this.hidePopup()
      });
  }

  getAssignmentSplitBoardDetails(skip ,limit){
    this.assignmentSplitBoardService.getAllAssignmentSplitBoardDetail(skip,limit)
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentSplitBoardList=data.reverse()
      });
  }

  editIntermediatePopUp(splitboardObj){
   this.EditOpenPopup()
    this.isEdit=true
    this.assignmentSplitBoardObj=splitboardObj

  }

  updateAssignmentSplitBoard(){
    this.assignmentSplitBoardService.updateAssignmentSplitBoardDetailById(this.assignmentSplitBoardObj['_id'],this.assignmentSplitBoardObj)
      .subscribe((data: any) => {
        console.log(data);
        // this.getAssignmentSplitBoardDetails(0,10);
        this.hidePopup()
      });
  }

  deleteIntermediatePopUp(splitboardObj){
    this.deletesplitboardObj=splitboardObj
     this.openDeletePopup()
  }

  deleteAssignmentSplitBoard(splitboardObj){
    this.assignmentSplitBoardService.deleteAssignmentSplitBoardDetailByMongodbId(splitboardObj['_id'])
      .subscribe((data: any) => {
        console.log(data);
        this.getAssignmentSplitBoardDetails(0,10);
        this.closeDeletePopup()
      });
  }
  EditOpenPopup(){

    $("#createAssignmentSplitBoard").removeClass("DN")
    $("#createAssignmentSplitBoard").addClass("DB")
  }

  openPopup(){
  this.isEdit=false
  this.assignmentSplitBoardObj={}
    $("#createAssignmentSplitBoard").removeClass("DN")
    $("#createAssignmentSplitBoard").addClass("DB")
  }
  hidePopup(){
    $("#createAssignmentSplitBoard").addClass("DN")
    $("#createAssignmentSplitBoard").removeClass("DB")
    this.getAssignmentSplitBoardDetails(0,10);
  }
  openDeletePopup() {

    $("#deleteAssignmentSplitBoard").removeClass("DN")
    $("#deleteAssignmentSplitBoard").addClass("DB")
  }

  closeDeletePopup() {

    $("#deleteAssignmentSplitBoard").addClass("DN")
    $("#deleteAssignmentSplitBoard").removeClass("DB")
  }
}
