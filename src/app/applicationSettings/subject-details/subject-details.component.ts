import { Component, OnInit } from '@angular/core';
import {SubjectDetailsService} from "../../services/applicationService/subject-details.service";
declare var $: any;
@Component({
  selector: 'app-subject-details',
  templateUrl: './subject-details.component.html',
  styleUrls: ['./subject-details.component.css']
})
export class SubjectDetailsComponent implements OnInit {

assignmentBadgeObj:any={}
subjectDetailsList:any=[]
isEdit:boolean=false
deletebadgeObj:any={}

constructor(public subjectDetailsService:SubjectDetailsService) { }

  ngOnInit() {
    this.getAssignmentBadgeDetails(0,10);

  }
  navigateToCreateSubjectDetailsPopup(){
    this.isEdit=false
    $("#createSubjectDetails").addClass("DB")
  }


  fnHidePop(){
    $("#createSubjectDetails").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }

  fnDeleteHidePop(){
    $("#deleteSubjectDetails").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }
  saveSubjectDetails(){

    this.subjectDetailsService.saveSubjectDetails(this.assignmentBadgeObj)
      .subscribe((data: any) => {
        console.log(data);
        this.subjectDetailsList.unshift(data)
        this.hidePopup()
      });
  }

  getAssignmentBadgeDetails(skip ,limit){
    this.subjectDetailsService.getAllSubject(skip,limit)
      .subscribe((data: any) => {
        console.log(data);
        this.subjectDetailsList=data.reverse()
      });
  }

  editIntermediatePopUp(badgeObj){
    this.isEdit=true
    this.assignmentBadgeObj=badgeObj
     this.EditOpenPopup()
  }

  updateSubjectDetails(){
    this.subjectDetailsService.updateSubjectById(this.assignmentBadgeObj['_id'],this.assignmentBadgeObj)
      .subscribe((data: any) => {
        console.log(data);
        this.hidePopup()
      });
  }

  deleteIntermediatePopUp(badgeObj){
    this.deletebadgeObj=badgeObj
    this.openDeletePopup()
  }

  deleteAssignmentBadge(badgeObj){
    this.subjectDetailsService.deleteSubjectByMongodbId(badgeObj['_id'])
      .subscribe((data: any) => {
        console.log(data);
        this.getAssignmentBadgeDetails(0,10);
        this.closeDeletePopup()
      });
  }
  EditOpenPopup(){

    $("#createSubjectDetails").removeClass("DN")
    $("#createSubjectDetails").addClass("DB")
  }

  openPopup(){
    this.isEdit=false
    this.assignmentBadgeObj={}
    $("#createSubjectDetails").removeClass("DN")
    $("#createSubjectDetails").addClass("DB")
  }
  hidePopup(){
    $("#createSubjectDetails").addClass("DN")
    $("#createSubjectDetails").removeClass("DB")
    this.getAssignmentBadgeDetails(0,10);
  }
  openDeletePopup() {

    $("#deleteSubjectDetails").removeClass("DN")
    $("#deleteSubjectDetails").addClass("DB")
  }

  closeDeletePopup() {

    $("#deleteSubjectDetails").addClass("DN")
    $("#deleteSubjectDetails").removeClass("DB")
  }

}
