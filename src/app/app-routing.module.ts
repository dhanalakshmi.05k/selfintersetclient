import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {DashboardCartViewComponent} from "./assignmentModule/dashboard-cart-view/dashboard-cart-view.component";
import {LoginComponent} from "./loginModule/login/login.component";
import {AssigmentListViewComponent} from "./assignmentModule/assigment-list-view/assigment-list-view.component";
import {CreateassigmentComponent} from "./assignmentModule/createassigment/createassigment.component";
import {AssignmentBadgeComponent} from "./applicationSettings/assignment-badge/assignment-badge.component";
import {AssignmentCategoryComponent} from "./applicationSettings/assignment-category/assignment-category.component";
import {YearDetailsComponent} from "./applicationSettings/year-details/year-details.component";
import {SemesterDetailsComponent} from "./applicationSettings/semester-details/semester-details.component";
import {AssignmentReasonComponent} from "./applicationSettings/assignment-reason/assignment-reason.component";
import {AssignmentLevelOfProficiencyComponent} from "./applicationSettings/assignment-level-of-proficiency/assignment-level-of-proficiency.component";
import {AssignmentSkillDetailComponent} from "./applicationSettings/assignment-skill-detail/assignment-skill-detail.component";
import {AssignmentSplitBoardComponent} from "./applicationSettings/assignment-split-board/assignment-split-board.component";
import {AssignmentTypeComponent} from "./applicationSettings/assignment-type/assignment-type.component";
import {AssignmentWeightComponent} from "./applicationSettings/assignment-weight/assignment-weight.component";
import {SettingsDashboardComponent} from "./applicationSettings/settings-dashboard/settings-dashboard.component";
import {GradingToolComponent} from "./applicationSettings/grading-tool/grading-tool.component";
import {GradingTypeComponent} from "./applicationSettings/grading-type/grading-type.component";
import {TeacherBadgeComponent} from "./applicationSettings/teacher-badge/teacher-badge.component";
import {HabitsOfLearnerComponent} from "./applicationSettings/habits-of-learner/habits-of-learner.component";
import {AssignmentFocusComponent} from "./applicationSettings/assignment-focus/assignment-focus.component";
import {CreateassigmentaddquestionComponent} from "./assignmentModule/createassigmentaddquestion/createassigmentaddquestion.component";
import {CreateassigmentgroupComponent} from "./assignmentModule/createassigmentgroup/createassigmentgroup.component";
import {AssignmentpublishComponent} from "./assignmentModule/assignmentpublish/assignmentpublish.component";
import {ForgotPasswordComponent} from "./loginModule/forgot-password/forgot-password.component";
import {SignUpComponent} from 'src/app/loginModule/sign-up/sign-up.component';
import {ClassDetailsComponent} from 'src/app/applicationSettings/class-details/class-details.component';
import {SubjectDetailsComponent} from 'src/app/applicationSettings/subject-details/subject-details.component';
import {UserManagementComponent} from 'src/app/applicationSettings/user-management/user-management.component';
import {QuestionsListViewComponent} from './assignmentModule/questions-list-view/questions-list-view.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {path: 'login', component: LoginComponent},
  {path: 'mainDashBoard', component: DashboardCartViewComponent},
  {path: 'assignmentListView', component: AssigmentListViewComponent},
  {path: 'createAssignment', component: CreateassigmentComponent},
  {path: 'createAssignment/:id', component: CreateassigmentComponent},
  {path: 'showAssignmentQuestions/:id', component: QuestionsListViewComponent},
  {path: 'applicationConfigSettings', component: SettingsDashboardComponent},
  {path: 'assignmentBadge', component: AssignmentBadgeComponent},
  {path: 'assignmentCategory', component: AssignmentCategoryComponent},
  {path: 'assignmentLevelOfProficiency', component: AssignmentLevelOfProficiencyComponent},
  {path: 'assignmentReason', component: AssignmentReasonComponent},
  {path: 'assignmentSkillDetail', component: AssignmentSkillDetailComponent},
  {path: 'assignmentSplitBoard', component: AssignmentSplitBoardComponent},
  {path: 'assignmentType', component: AssignmentTypeComponent},
  {path: 'assignmentWeight', component: AssignmentWeightComponent},
  {path: 'semesterDetails', component: SemesterDetailsComponent},
  {path: 'yearDetails', component: YearDetailsComponent},
  {path: 'gradingTool', component: GradingToolComponent},
  {path: 'gradingType', component: GradingTypeComponent},
  {path: 'TeacherBadge', component: TeacherBadgeComponent},
  {path: 'habitsOfLearner', component: HabitsOfLearnerComponent},
  {path: 'assignmentFocus', component: AssignmentFocusComponent},
  {path: 'createQuestion/:id', component: CreateassigmentaddquestionComponent},
  {path: 'createGroup/:id', component: CreateassigmentgroupComponent},
  {path: 'publishAssignment/:id/:id2', component: AssignmentpublishComponent},
  {path: 'forgotPassword', component: ForgotPasswordComponent},
  {path: 'signUp', component: SignUpComponent},
  {path: 'classDetails', component: ClassDetailsComponent},
  {path: 'subjectDetails', component: SubjectDetailsComponent},
  {path: 'userManagement', component: UserManagementComponent},
  {path: 'assignmentFinalMarkBy', component: AssignmentCategoryComponent},

];






@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})


export class AppRoutingModule {}
